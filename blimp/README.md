# Blimp

Blimp is an experimental project for building Chromium as a thin client.

## Engineering

For building blimp, read more at [building](docs/build.md).
