{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "children": [
        {
          "position": [10, 10],
          "transformOrigin": [60, 60],
          "bounds": [270, 170],
          "drawsContent": true,
          "backgroundColor": "#008000",
          "children": [
            {
              "position": [50, 50],
              "bounds": [220, 120],
              "drawsContent": true,
              "backgroundColor": "#FF000099"
            }
          ]
        },
        {
          "position": [75, 75],
          "bounds": [56, 56],
          "contentsOpaque": true,
          "drawsContent": true,
          "backgroundColor": "#0000FF"
        }
      ]
    }
  ]
}

