{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "repaintRects": [
        [68, 119, 52, 18],
        [26, 137, 40, 18],
        [26, 119, 43, 18]
      ],
      "paintInvalidationClients": [
        "LayoutBlockFlow DIV id='overflow'",
        "LayoutText #text",
        "InlineTextBox 'Lorem'",
        "LayoutBR BR",
        "InlineTextBox '\n'",
        "LayoutText #text",
        "InlineTextBox 'ipsum'"
      ]
    }
  ]
}

