{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "repaintRects": [
        [8, 150, 784, 18],
        [8, 150, 39, 18],
        [8, 150, 36, 18]
      ],
      "paintInvalidationClients": [
        "InlineTextBox ''",
        "RootInlineBox",
        "LayoutBlockFlow (relative positioned) DIV id='target'",
        "LayoutText #text",
        "InlineTextBox 'PASS'"
      ]
    }
  ]
}

