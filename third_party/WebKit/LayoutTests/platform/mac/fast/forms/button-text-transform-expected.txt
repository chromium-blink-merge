layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x576
      LayoutBlockFlow {P} at (0,0) size 784x36
        LayoutText {#text} at (0,0) size 55x18
          text run at (0,0) width 55: "Test for "
        LayoutInline {I} at (0,0) size 760x36
          LayoutInline {A} at (0,0) size 306x18 [color=#0000EE]
            LayoutText {#text} at (54,0) size 306x18
              text run at (54,0) width 306: "http://bugs.webkit.org/show_bug.cgi?id=15181"
          LayoutText {#text} at (359,0) size 760x36
            text run at (359,0) width 5: " "
            text run at (363,0) width 397: "text-transform: uppercase not working in input (submit, reset,"
            text run at (0,18) width 107: "button) elements"
        LayoutText {#text} at (106,18) size 5x18
          text run at (106,18) width 5: "."
      LayoutBlockFlow {P} at (0,52) size 784x19
        LayoutButton {BUTTON} at (0,1) size 85.16x18 [bgcolor=#C0C0C0] [border: none (2px outset #C0C0C0) none (2px outset #C0C0C0)]
          LayoutBlockFlow (anonymous) at (8,2) size 69.16x13
            LayoutText {#text} at (0,0) size 70x13
              text run at (0,0) width 70: "UPPERCASE"
        LayoutText {#text} at (85,0) size 5x18
          text run at (85,0) width 5: " "
        LayoutButton {BUTTON} at (89.16,1) size 69.42x18 [bgcolor=#C0C0C0] [border: none (2px outset #C0C0C0) none (2px outset #C0C0C0)]
          LayoutBlockFlow (anonymous) at (8,2) size 53.42x13
            LayoutText {#text} at (0,0) size 54x13
              text run at (0,0) width 54: "lowercase"
        LayoutText {#text} at (158,0) size 5x18
          text run at (158,0) width 5: " "
        LayoutButton {BUTTON} at (162.58,1) size 68.06x18 [bgcolor=#C0C0C0] [border: none (2px outset #C0C0C0) none (2px outset #C0C0C0)]
          LayoutBlockFlow (anonymous) at (8,2) size 52.06x13
            LayoutText {#text} at (0,0) size 53x13
              text run at (0,0) width 53: "Capitalize"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {P} at (0,87) size 784x19
        LayoutButton {INPUT} at (0,1) size 85.16x18 [bgcolor=#C0C0C0]
          LayoutBlockFlow (anonymous) at (8,2) size 69.16x13
            LayoutText {#text} at (0,0) size 70x13
              text run at (0,0) width 70: "UPPERCASE"
        LayoutText {#text} at (85,0) size 5x18
          text run at (85,0) width 5: " "
        LayoutButton {INPUT} at (89.16,1) size 69.42x18 [bgcolor=#C0C0C0]
          LayoutBlockFlow (anonymous) at (8,2) size 53.42x13
            LayoutText {#text} at (0,0) size 54x13
              text run at (0,0) width 54: "lowercase"
        LayoutText {#text} at (158,0) size 5x18
          text run at (158,0) width 5: " "
        LayoutButton {INPUT} at (162.58,1) size 68.06x18 [bgcolor=#C0C0C0]
          LayoutBlockFlow (anonymous) at (8,2) size 52.06x13
            LayoutText {#text} at (0,0) size 53x13
              text run at (0,0) width 53: "Capitalize"
        LayoutText {#text} at (0,0) size 0x0
