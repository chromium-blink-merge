layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x576
      LayoutBlockFlow {P} at (0,0) size 784x18
        LayoutText {#text} at (0,0) size 55x18
          text run at (0,0) width 55: "Test for "
        LayoutInline {I} at (0,0) size 582x18
          LayoutInline {A} at (0,0) size 306x18 [color=#0000EE]
            LayoutText {#text} at (54,0) size 306x18
              text run at (54,0) width 306: "http://bugs.webkit.org/show_bug.cgi?id=11362"
          LayoutText {#text} at (359,0) size 277x18
            text run at (359,0) width 5: " "
            text run at (363,0) width 273: "Native popup with size=\"1\" wraps options"
        LayoutText {#text} at (635,0) size 5x18
          text run at (635,0) width 5: "."
      LayoutBlockFlow {P} at (0,34) size 784x18
        LayoutText {#text} at (0,0) size 36x18
          text run at (0,0) width 36: "With "
        LayoutInline {TT} at (0,0) size 64x15
          LayoutText {#text} at (35,2) size 64x15
            text run at (35,2) width 64: "size=\"1\""
        LayoutText {#text} at (98,0) size 9x18
          text run at (98,0) width 9: ": "
        LayoutMenuList {SELECT} at (106.83,2) size 100x15 [bgcolor=#F8F8F8] [border: (1px solid #A6A6A6)]
          LayoutBlockFlow (anonymous) at (1,1) size 98x13
            LayoutText (anonymous) at (0,0) size 174x13
              text run at (0,0) width 174: "Very long option that does not fit"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {P} at (0,68) size 784x18
        LayoutText {#text} at (0,0) size 57x18
          text run at (0,0) width 57: "Without "
        LayoutInline {TT} at (0,0) size 32x15
          LayoutText {#text} at (56,2) size 32x15
            text run at (56,2) width 32: "size"
        LayoutText {#text} at (87,0) size 10x18
          text run at (87,0) width 10: ": "
        LayoutMenuList {SELECT} at (96.08,2) size 100x15 [bgcolor=#F8F8F8] [border: (1px solid #A6A6A6)]
          LayoutBlockFlow (anonymous) at (1,1) size 98x13
            LayoutText (anonymous) at (0,0) size 174x13
              text run at (0,0) width 174: "Very long option that does not fit"
        LayoutText {#text} at (0,0) size 0x0
