layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x576
      LayoutBlockFlow {H2} at (0,0) size 784x28
        LayoutText {#text} at (0,0) size 364x28
          text run at (0,0) width 364: "SeaMonkey XPInstall Trigger Page"
      LayoutBlockFlow {HR} at (0,47.91) size 784x2 [border: (1px inset #EEEEEE)]
      LayoutBlockFlow {FORM} at (0,57.91) size 784x47
        LayoutTable {TABLE} at (0,0) size 602x47
          LayoutTableSection {TBODY} at (0,0) size 602x47
            LayoutTableRow {TR} at (0,2) size 602x21
              LayoutTableCell {TD} at (2,2) size 107x20 [r=0 c=0 rs=1 cs=1]
                LayoutInline {B} at (0,0) size 96x18
                  LayoutText {#text} at (1,1) size 96x18
                    text run at (1,1) width 96: "Trigger URL:"
              LayoutTableCell {TD} at (111,2) size 403x21 [r=0 c=1 rs=1 cs=1]
                LayoutTextControl {INPUT} at (1,1) size 401x19 [bgcolor=#FFFFFF] [border: (2px inset #EEEEEE)]
              LayoutTableCell {TD} at (516,2) size 84x20 [r=0 c=2 rs=1 cs=1]
                LayoutButton {INPUT} at (1,1) size 53.30x18 [bgcolor=#C0C0C0]
                  LayoutBlockFlow (anonymous) at (8,2) size 37.30x13
                    LayoutText {#text} at (0,0) size 38x13
                      text run at (0,0) width 38: "Trigger"
            LayoutTableRow {TR} at (0,25) size 602x20
              LayoutTableCell {TD} at (2,25) size 107x20 [r=1 c=0 rs=1 cs=1]
                LayoutInline {B} at (0,0) size 105x18
                  LayoutText {#text} at (1,1) size 105x18
                    text run at (1,1) width 105: "Run Test Case:"
              LayoutTableCell {TD} at (111,25) size 403x20 [r=1 c=1 rs=1 cs=1]
                LayoutMenuList {SELECT} at (1,1) size 259x18 [bgcolor=#F8F8F8]
                  LayoutBlockFlow (anonymous) at (0,0) size 259x18
                    LayoutText (anonymous) at (8,2) size 71x13
                      text run at (8,2) width 71: "a_abortinstall"
                LayoutText {#text} at (0,0) size 0x0
              LayoutTableCell {TD} at (516,25) size 84x20 [r=1 c=2 rs=1 cs=1]
                LayoutButton {INPUT} at (1,1) size 81.52x18 [bgcolor=#C0C0C0]
                  LayoutBlockFlow (anonymous) at (8,2) size 65.52x13
                    LayoutText {#text} at (0,0) size 66x13
                      text run at (0,0) width 66: "Trigger case"
layer at (123,72) size 395x13
  LayoutBlockFlow {DIV} at (3,3) size 395x13
    LayoutText {#text} at (0,0) size 96x13
      text run at (0,0) width 96: "http://jimbob/jars/"
